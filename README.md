# Docker images for data analysis

Create docker images for data analysis.
This project allows to build images also with different `TexLive` versions.
Images are installed using [kaniko](https://github.com/GoogleContainerTools/kaniko).
The build procedure is copied from [https://gitlab.cern.ch/ci-tools/ci-worker](https://gitlab.cern.ch/ci-tools/ci-worker), and the base images are obtained from [https://linux.web.cern.ch/dockerimages](https://linux.web.cern.ch/dockerimages).

In order to contribute to the package, you must fork it and create a merge request. In order to build a new image, modify [.gitlab-ci.yml](.gitlab-ci.yml) including new steps, whose content must be similar to:

```yaml
cc7:
  extends: .build-base
  variables:
    DOCKERFILE: Dockerfile.cc7
    BASE_IMAGE: gitlab-registry.cern.ch/linuxsupport/cc7-base
    DESTINATION: $CI_REGISTRY/cc7:latest

cc7-texlive-2020:
  extends: .build-texlive
  variables:
    DOCKERFILE: Dockerfile.tgt
    BASE_IMAGE: $CI_REGISTRY/cc7:latest
    DESTINATION: $CI_REGISTRY/cc7-texlive:2020
    TEXLIVE_VERSION: 2020
    TEXLIVE_URL: https://ftp.math.utah.edu/pub/tex/historic/systems/texlive/$TEXLIVE_VERSION/tlnet-final
  needs:
    - job: cc7

cc7-texlive-latest:
  extends: .build-texlive-latest
  variables:
    BASE_IMAGE: $CI_REGISTRY/cc7:latest
    DESTINATION: $CI_REGISTRY/cc7-texlive:latest
  needs:
    - job: cc7
```
Jobs generating base images must extend `build-base`, hereas jobs including the `TeXLive` installation must depend on `build-texlive` or `build-texlive-latest`. This allows to build the images using the instructions in the `Dockerfile` files under the `ci-base` and `ci-texlive` directories. The documentation on the `Dockerfile` instructions can be found in [https://docs.docker.com/engine/reference/builder](https://docs.docker.com/engine/reference/builder).

## Base images

In this repository, a base image is considered to be an image taken from [https://linux.web.cern.ch/dockerimages](https://linux.web.cern.ch/dockerimages) which has been modified (including additional packages, changing the environment, ...).
To provide the instructions to build a base image, you must modify `.gitlab-ci.yml` and include an additional job like the following:

```yaml
cc7:
  extends: .build-base
  variables:
    DOCKERFILE: Dockerfile.cc7
    BASE_IMAGE: gitlab-registry.cern.ch/linuxsupport/cc7-base
    DESTINATION: $CI_REGISTRY/cc7:latest
```

The job configuration must extend the `.build-base` configuration. In addition, you must provide the `Dockerfile` that is going to be used in the `DOCKERFILE` variable, which must be placed in the `ci-base` directory. The base image must be provided in the `BASE_IMAGE` variable, while the name and tag of the produced image is specified in `DESTINATION`.

## Images with a TeXLive installation

It is possible to produce images with `TeXLive` installed, whose version can correspond to the latest available or a particular year. If you simply want to build images with the latest version available, the configuration of your job must look like:

```yaml
cc7-texlive-latest:
  extends: .build-texlive-latest
  variables:
    BASE_IMAGE: $CI_REGISTRY/cc7:latest
    DESTINATION: $CI_REGISTRY/cc7-texlive:latest
  needs:
    - job: cc7
```

In this case the configuration is very simple. We simply must extend the `.build-texlive-latest` configuration and ensure that if the base image is created in the same pipeline the dependencies are correctly specified filling the `needs` field.

On the other hand, with the purpose of maintaining backwards compatibility, it is possible to make a build with a specific version of  `TeXLive`. In this case the configuration must look like:

```yaml
cc7-texlive-2020:
  extends: .build-texlive
  variables:
    DOCKERFILE: Dockerfile.tgt
    BASE_IMAGE: $CI_REGISTRY/cc7:latest
    DESTINATION: $CI_REGISTRY/cc7-texlive:2020
    TEXLIVE_VERSION: 2020
    TEXLIVE_URL: https://ftp.math.utah.edu/pub/tex/historic/systems/texlive/$TEXLIVE_VERSION/tlnet-final
  needs:
    - job: cc7
```

In this case you must extend the `.build-texlive` configuration and also provide the `TexLive` version (year) together with the URL to the repository in which the source is stored.

## Building procedure

By default, all images are built within a pipeline, which means that concurrent pipelines will interfere with each other (since all write to the same registry). To force the build of a single image, a label with the name of the image
must be created, and the corresponding merge request must be assigned that
label. The label `documentation` is reserved for modifications of [README.md](README.md).

The option `ci_allow_fork_pipelines_to_run_in_parent_project`, introduced in `GitLab 15.3` and set to `true` by default, has been disabled.
This has been done by making a temporary token with write-access to the project's API and running the command

```bash
curl --request PUT --header "PRIVATE-TOKEN: <your-token>" \
 --url "https://gitlab.cern.ch/api/v4/projects/103680" \
 --data "ci_allow_fork_pipelines_to_run_in_parent_project=false"
```

By doing this, the docker images are not installed in the registry of this repository but on that of the fork.

## Archived images

With time, certain images become obsolete since the Linux team at CERN stop giving support for them.
Some of these images might still be available, but they are prone to issues due to changes produced upstream (in the base images of e.g. CentOS 7) that are not handled correctly in the images created downstream.
Therefore, once one of the builds is defined as "obsolete", the associated image in this repository must be put in a permanent static location, which is [Docker Hub](https://hub.docker.com).
Afterwards the build in this repository must be removed, and the image in the container registry must be simply that of the stored static image.
